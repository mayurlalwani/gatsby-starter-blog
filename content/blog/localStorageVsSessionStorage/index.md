---
title: localStorage VS sessionStorage
date: "2020-07-06T22:12:03.284Z"
description: "In this blog I will discuss about localStorage and sessionStorage"
---

This is my first post on my new fake blog! How exciting!

Both localStorage and sessionStorage are web storage APIs that can be used to save data in the browser.

## Some localStorage features

- Data never gets expired. It will remain even after closing the tab, or even the browser.
- Data is shared between tabs and windows from the same origin.

## Session Storage

- The sessionStorage is used much less than localStorage. The data is only stored for a particular session. For example, once the tab or browser is closed, the data is cleared.

- The data survives page refresh.