---
title: Debounce and Throttle!
date: "2020-07-05T23:46:37.121Z"
---

Wow! I love blogging so much already.

Debouncing and throttling are recommended to use on events that a user can fire more often than you need them to. These are not provided by JavaScript itself, but it can be implemented using a setTimeout web API. But, some libraries like underscore.js and lodash provides these methods which can be easy to implement.

# Debounce

In the debouncing technique, no matter how many times the user fires an event, the attached function will be executed only after the specified time once the user stops firing the event.

# Throttle

Throttling is a technique in which, no matter how many times the user fires the event, the attached function will be executed only once in a given time interval.